import { NamedRoutersXPage } from './app.po';

describe('named-routers-x App', () => {
  let page: NamedRoutersXPage;

  beforeEach(() => {
    page = new NamedRoutersXPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
