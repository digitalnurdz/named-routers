import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-primary',
  templateUrl: './primary.component.html',
  styleUrls: ['./primary.component.scss']
})
export class PrimaryComponent implements OnInit {

  public name: any = 'primaryview';
  constructor() { }

  ngOnInit() {
  }

}
