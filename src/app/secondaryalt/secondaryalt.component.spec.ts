import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SecondaryaltComponent } from './secondaryalt.component';

describe('SecondaryaltComponent', () => {
  let component: SecondaryaltComponent;
  let fixture: ComponentFixture<SecondaryaltComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SecondaryaltComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SecondaryaltComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
