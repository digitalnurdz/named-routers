import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrimaryaltComponent } from './primaryalt.component';

describe('PrimaryaltComponent', () => {
  let component: PrimaryaltComponent;
  let fixture: ComponentFixture<PrimaryaltComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrimaryaltComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrimaryaltComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
