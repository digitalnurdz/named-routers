import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import {RouterModule, Routes} from '@angular/router';
import { MainComponent } from './main/main.component';
import { SecondaryComponent } from './secondary/secondary.component';
import { PrimaryComponent } from './primary/primary.component';
import {MdCardModule} from '@angular/material';
import { PrimaryaltComponent } from './primaryalt/primaryalt.component';
import { SecondaryaltComponent } from './secondaryalt/secondaryalt.component';

const appRoutes: Routes = [
  {
    path: '',
    redirectTo: 'app-main',
    pathMatch: 'full'

  },
  {
    path: 'app-main',
    component: MainComponent,
    children: [
      { path: 'app-primary', component: PrimaryComponent, outlet: 'primary' },
      { path: 'app-secondary', component: SecondaryComponent, outlet: 'secondary' },
      { path: 'app-primaryalt', component: PrimaryaltComponent, outlet: 'primary' },
      { path: 'app-secondaryalt', component: SecondaryaltComponent, outlet: 'secondary' },
      /*{ path: 'needing-attention', component: NeedingAttentionComponent, outlet: 'ecp' },
      { path: 'delayed-orders', component: DelayedOrdersComponent, outlet: 'ecp' },
      { path: 'completed-orders', component: CompletedOrdersComponent, outlet: 'ecp' },
      { path: 'search-view', component: SearchViewComponent, outlet: 'ecp' },
      { path: 'app-reports-open', component: ReportsOpenComponent, outlet: 'reports' },
      { path: 'app-reports-closed', component: ReportsClosedComponent, outlet: 'reports' }*/
    ]
  }
];

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    SecondaryComponent,
    PrimaryComponent,
    PrimaryaltComponent,
    SecondaryaltComponent
  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    FormsModule,
    HttpModule,
    MdCardModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
