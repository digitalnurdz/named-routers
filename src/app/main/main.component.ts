import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  private locationView: any;

  constructor(public router: Router) {
  }

  ngOnInit() {
    this.router.navigate(['/app-main', {outlets: {'primary': ['app-primary'], 'secondary' : ['app-secondary']}}]);
  }

  swapRoutes () {
    console.log('where >>>> ', this.locationView);
    if (this.locationView === 'primaryview') {
      this.router.navigate(['/app-main', {outlets: {'primary': ['app-primaryalt'], 'secondary' : ['app-secondaryalt']}}]);
    } else {
      this.router.navigate(['/app-main', {outlets: {'primary': ['app-primary'], 'secondary' : ['app-secondary']}}]);
    }
  }

  onActivate(event) {
    let evt = event['name'];
    this.locationView = evt;


    /*console.log('>>>> ', event);
    switch (evt) {
      case 'primaryview':
        this.locationView = evt;
        break;
    }*/
  }
}
